# URL Shortening service

## DESIGN

### Functional Requirements

- Given a valid URL a shortened URL is created for easy sharing. 
- The shortened URL redirects immediately to the original (longer) URL.

### Non Functional Requirements
- Application should support more than one agent.
- Short URL has random token (non guessable).

## Services available

The final application will provide a POST endpoint for short URL creation and a GET endpoint for URL retrieval and automatic redirection.

### Considerations over the URL input by the user
- Only valid URLs will be shortened
- Only not already shortened URLs by this same service will be shortened

### Token considerations
Token length is 6 characters resulting after applying base64 encoding to the resulting MD5 hash (from input URL). This gives 64^6 possible tokens (~68.7 billion).

## Developer Information

### Requirements

- Java JDK 11
- Maven 3.6 ^
- Docker Linux (optional)


This project was created using [Spring Initializr](https://start.spring.io/)


### Building and testing the solution

Build:
```bash
 mvn clean package 
 ```

Only Test:
```bash
mvn test
```

Run locally with Spring:
```bash
.\mvnw spring-boot:run
```

Build docker image
``` bash
 docker build -t assignment/shortener .
```

Run docker image
``` bash
docker run --name shortener -p<yourPort>:8080 assignment/shortener
```

Create a link with curl
```bash
curl -X POST localhost:<yourPort>/create -H 'Content-type:application/json' -d '{"longURL": "https://elpais.com"}'

# Sample response:
#
# {
#    "token":"smbL2g",
#    "shortURL":"localhost/smbL2g",
#    "longURL":"https://elpais.com",
#    "creation":"2021-05-10T08:19:17.820+00:00",
#    "lastUsed":null
# }
```

Check the created link
```bash
curl -i -X GET localhost:<yourPort>/yOQHG

# Sample response:
#
# HTTP/1.1 302 
# Location: https://elpais.com       
# Content-Language: en
# Content-Length: 0
# Date: Mon, 10 May 2021 08:20:32 GMT
```

### Proposed next steps
#### Using AWS stack:
- Deploy this solution into an ECS task 
- Proxy access to the API with API gateway and assign API key
- Use XRay with API gateway to gather statistics and decouple this side goal from main functionality
- Create a cloudformation stack for automatic rollout of this solution
- Create a lambda function to trigger stack duplication in orther to allow payed customers to have their own separated 'instances' with custom domains.
#### Other
- Decouple the token generation into its own Key Generation Service to avoid key duplication and its consequent DB overhead of introducing such checks.
- Implement real Database persistance
- Build nice front-end to present the service (?)