package com.onesockpirates.assignment.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.onesockpirates.assignment.models.URLMapping;
import com.onesockpirates.assignment.models.URLMappingRequest;

import java.net.URL;

import com.onesockpirates.assignment.managers.IURLMappingManager;

@RestController
public class URLController {

  private final IURLMappingManager urlManager;

  @Autowired
  public URLController(IURLMappingManager manager) {
    this.urlManager = manager;
  }

  @GetMapping("/")
  public String index() {
    return "Welcome to the URL shortener service";
  }

  @PostMapping("/create")
  public ResponseEntity<URLMapping> newShortURL(@RequestBody URLMappingRequest request){
    String url = request.getLongURL();
    return new ResponseEntity<URLMapping>(this.urlManager.createNew(url), HttpStatus.CREATED);
  }

  @GetMapping("/{token}") 
  public ModelAndView getOne(@PathVariable String token) {
    URL url = this.urlManager.getMapping(token);
    return new ModelAndView("redirect:" + url);
  }

}