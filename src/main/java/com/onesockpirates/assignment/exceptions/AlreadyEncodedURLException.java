package com.onesockpirates.assignment.exceptions;

public class AlreadyEncodedURLException extends RuntimeException {

  public AlreadyEncodedURLException(String url) {
    super("User is trying to encode already encoded url: " + url);
  }
}