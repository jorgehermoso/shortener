package com.onesockpirates.assignment.exceptions;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class CustomExceptionHandler {
    
    public class APIException{
        private String message;
        private Date time;

        public APIException (String message){
            this.message = message;
            this.time = new Date();
        }

        public String getMessage(){
            return this.message;
        }
        public Date getTime(){
            return this.time;
        }
    }

    @ExceptionHandler(value = {NotAValidURLException.class})
    public ResponseEntity<Object> handleCreateRequestExceptions (NotAValidURLException e){
        return new ResponseEntity<>(
            new APIException(e.getMessage()),
            HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(value = {AlreadyEncodedURLException.class})
    public ResponseEntity<Object> handleCreateRequestExceptions (AlreadyEncodedURLException e){
        return new ResponseEntity<>(
            new APIException(e.getMessage()),
            HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(value = {InternalProblemException.class})
    public ResponseEntity<Object> handleCreateRequestExceptions (InternalProblemException e){
        return new ResponseEntity<>(
            new APIException("There was an internal service malfunction."),
            HttpStatus.INTERNAL_SERVER_ERROR
        );
    }

    @ExceptionHandler(value = {NotAValidTokenException.class})
    public ResponseEntity<Object> handleCreateRequestExceptions (NotAValidTokenException e){
        return new ResponseEntity<>(
            new APIException(e.getMessage()),
            HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(value = {NotUniqueIDException.class})
    public ResponseEntity<Object> handleCreateRequestExceptions (NotUniqueIDException e){
        return new ResponseEntity<>(
            new APIException(e.getMessage()),
            HttpStatus.BAD_REQUEST
        );
    }
    @ExceptionHandler(value = {URLNotFoundException.class})
    public ResponseEntity<Object> handleCreateRequestExceptions (URLNotFoundException e){
        return new ResponseEntity<>(
            new APIException(e.getMessage()),
            HttpStatus.NOT_FOUND
        );
    }
}
