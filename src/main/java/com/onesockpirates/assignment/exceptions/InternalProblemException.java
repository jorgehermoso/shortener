package com.onesockpirates.assignment.exceptions;

public class InternalProblemException extends RuntimeException {

  public InternalProblemException() {
    super("Something went terrible wrong!");
  }
}