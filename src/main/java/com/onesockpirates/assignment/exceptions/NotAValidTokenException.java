package com.onesockpirates.assignment.exceptions;

public class NotAValidTokenException extends RuntimeException {

  public NotAValidTokenException(String token) {
    super("User input non conforming token: " + token);
  }
}