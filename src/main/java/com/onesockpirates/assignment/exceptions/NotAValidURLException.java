package com.onesockpirates.assignment.exceptions;

public class NotAValidURLException extends RuntimeException {

  public NotAValidURLException(String url) {
    super("User did input not valid URL: " + url);
  }
}