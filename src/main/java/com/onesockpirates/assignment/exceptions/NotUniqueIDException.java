package com.onesockpirates.assignment.exceptions;

public class NotUniqueIDException extends RuntimeException {

  public NotUniqueIDException(String key) {
    super("An item already exists in DB with key = " + key);
  }
}