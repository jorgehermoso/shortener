package com.onesockpirates.assignment.exceptions;

public class URLNotFoundException extends RuntimeException {

  public URLNotFoundException(String token) {
    super("No short URL created with this token: " + token);
  }
}