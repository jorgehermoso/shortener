package com.onesockpirates.assignment.helpers;

import java.security.*;

public interface IHasher {

    public String uniqueHash(String source) throws NoSuchAlgorithmException;
    public String uniqueHash(String source, int trim) throws NoSuchAlgorithmException;

}
