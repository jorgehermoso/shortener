package com.onesockpirates.assignment.managers;

import com.onesockpirates.assignment.exceptions.NotUniqueIDException;

public interface IStorageManager<T> {
    public void intialize(String name, Class<T> type);
    public T save(T in) throws NotUniqueIDException;
    public T query(String queryFilter);
}