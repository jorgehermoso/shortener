package com.onesockpirates.assignment.managers;

import java.net.URL;

import com.onesockpirates.assignment.models.URLMapping;
import com.onesockpirates.assignment.exceptions.*;

public interface IURLMappingManager {
    
    public URLMapping createNew(String url) throws AlreadyEncodedURLException, NotAValidURLException, InternalProblemException;
    public URL getMapping(String token) throws NotAValidTokenException, URLNotFoundException;
}
