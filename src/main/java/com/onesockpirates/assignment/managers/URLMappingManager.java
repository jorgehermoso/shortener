package com.onesockpirates.assignment.managers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.regex.*;  
import java.util.Date;

import com.onesockpirates.assignment.helpers.IHasher;
import com.onesockpirates.assignment.models.URLMapping;
import com.onesockpirates.assignment.exceptions.*;
import com.onesockpirates.assignment.repositories.IURLRepository;

/** 
* Manages the access and modifications of the URLMappings
*/
@Component
public class URLMappingManager implements IURLMappingManager {

    private final IURLRepository repository;
    private final IHasher hasher;
    private String hostname;

    /**
     * Constructor
     * @param repository the repository to use
     * @param hasher the hasher implementation for token creation
     */
    @Autowired
    URLMappingManager(IURLRepository repository, IHasher hasher) {
        this.repository = repository;
        this.hasher = hasher;
        try {
            this.hostname = InetAddress.getLoopbackAddress().getHostName();
        } catch (Exception e){
            this.hostname = "unknown";
        }
    }

    /**
     * @param url a String with the url for which the mapping needs to be created
     * @return URLMapping with information of the mapping, including short URL
     * @throws AlreadyEncodedURLException
     * @throws NotAValidURLException
     * @throws InternalProblemException
     */
    public URLMapping createNew(String url) throws AlreadyEncodedURLException, NotAValidURLException, InternalProblemException{
        // Validate input (not empty, not already a link)
        String regex1 = "^https?://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
        String regex2 = "^https?://"+ hostname + "/[A-Za-z0-9+/]{6}";
        
        if ((url == "") || (!Pattern.matches(regex1, url))) {
            throw new NotAValidURLException(url);
        } else if (Pattern.matches(regex2, url)){
            throw new AlreadyEncodedURLException(url);
        }

        try {
            String hash = this.hasher.uniqueHash(url);
            URLMapping created = null;
            try {
                created = this.repository.save(
                    new URLMapping(url, hash.substring(0,6))
                );
            } catch (NotUniqueIDException e){
                // TODO Dev1: Implement proper procedure for duplicated keys
                created = this.repository.save(
                    new URLMapping(url, hash.substring(1,7))
                );
            }
            created.setShortURL(this.hostname + "/" + created.getToken());
            return created;
        } catch (NoSuchAlgorithmException e){
            throw new InternalProblemException();
        }
    }

    /**
     * Retrieves Mapping if it exists and updates DB record with last access date
     * @param token the key to look for (short URL)
     * @return (URL) the original URL for which the mapping was created
     * @throws NotAValidTokenException
     * @throws URLNotFoundException
     */
    public URL getMapping(String token) throws NotAValidTokenException, URLNotFoundException{
        String regex = "[A-Za-z0-9+/]{6}";
        if (!Pattern.matches(regex, token)) {
            throw new NotAValidTokenException(token);
        }
        URLMapping mapping = repository.findById(token);
        if (mapping == null) {
            throw new URLNotFoundException(token);
        }
        mapping.setLastUsed(new Date());
        repository.save(mapping);
        return mapping.getLongURL();
    }
    
}
