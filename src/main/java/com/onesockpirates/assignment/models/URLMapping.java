package com.onesockpirates.assignment.models;

import java.net.URL;
import java.util.Date;

public class URLMapping {

    private String token;
    private String shortURL;
    private URL longURL;
    private Date creation; 
    private Date lastUsed; 

    public URLMapping(){}

    public URLMapping( String longURL, String token){
        try{
            this.longURL = new URL(longURL);
            this.token = token;
            this.creation = new Date();
        } catch (Exception e) {
			e.printStackTrace();
        }
    }

    public String getToken() {
        return this.token;
    }
    public void setToken(String token){
        this.token = token;
    }

    public void setShortURL( String url) {
        this.shortURL = url;
    }

    public String getShortURL(){
        return this.shortURL;
    }

    public URL getLongURL() {
        return this.longURL;
    }

    public void setLongURL( URL longURL) {
        this.longURL = longURL;
    }

    public Date getCreation(){
        return this.creation;
    }

    public void setCreation( Date date){
        this.creation = date;
    }

    public Date getLastUsed(){
        return this.lastUsed;
    }

    public void setLastUsed(Date date){
        this.lastUsed = date;
    }

}