package com.onesockpirates.assignment.models;

public class URLMappingRequest {

    private String longURL;

    URLMappingRequest() {}

    public URLMappingRequest( String longURL) {
        this.longURL = longURL;
    }

    public String getLongURL() {
        return this.longURL;
    }

    public void setLongURL( String longURL) {
        this.longURL = longURL;
    }

}