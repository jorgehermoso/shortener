package com.onesockpirates.assignment.repositories;

import com.onesockpirates.assignment.models.URLMapping;
import com.onesockpirates.assignment.exceptions.NotUniqueIDException;
import org.springframework.stereotype.Component;

@Component
public interface IURLRepository {

    public URLMapping save(URLMapping mapping) throws NotUniqueIDException;
    public URLMapping findById(String token);
}