package com.onesockpirates.assignment.repositories;

import com.onesockpirates.assignment.models.URLMapping;
import com.onesockpirates.assignment.managers.IStorageManager;
import com.onesockpirates.assignment.exceptions.NotUniqueIDException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Manages the access to the persistence layer 
 * 
 */
@Component
public class URLRepository implements IURLRepository{

    private IStorageManager<URLMapping> database;  

    /**
     * Constructor
     * @param db storage interface implementation
     * 
     */
    @Autowired
    URLRepository(IStorageManager<URLMapping> db){
        this.database = db;
        this.database.intialize("Mappings", URLMapping.class);
    }

    /**
     * Persists an element of type URLMapping into the database
     * @param mapping the object to be persisted
     * @return (URLMapping) if succesful returns the same object
     * @throws NotUniquIDException if there is an ID conflict in the database
     * 
     */
    public URLMapping save(URLMapping mapping) throws NotUniqueIDException{
        return this.database.save(mapping);
    }

    /**
     * Find in the database a record with a particular token
     * @param token the token of the mapping to be retrieved
     * @return (URLMapping) if found returns mapping otherwise returns null
     * 
     */
    public URLMapping findById(String token){
        return this.database.query("\"token\":\"" + token + "\"");
    }
}