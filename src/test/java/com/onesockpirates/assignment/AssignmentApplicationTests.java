package com.onesockpirates.assignment;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

import com.onesockpirates.assignment.models.URLMapping;
import com.onesockpirates.assignment.models.URLMappingRequest;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;


@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
class AssignmentApplicationTests {

	@LocalServerPort
    int randomServerPort;

	@Autowired
    private TestRestTemplate restTemplate;

	@BeforeAll
	private static void setup(){
		File myObj = new File("databaseMappings.txt"); 
		if (myObj.delete()) { 
		System.out.println("Deleted database contents: ");
		} else {
		System.out.println("Failed to delete database contents.");
		} 
	}

	@Test
	void contextLoads() throws Exception {
	}

	@Test
	void POSTcall_WithCorrectData_ReturnsCreated() throws URISyntaxException{
		final String baseUrl = "http://localhost:"+randomServerPort+"/create";
        URI uri = new URI(baseUrl);
        URLMappingRequest requestData = new URLMappingRequest("http://google.com");
        HttpEntity<URLMappingRequest> request = new HttpEntity<>(requestData);
        ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);
        assertEquals(201, result.getStatusCodeValue());
	}

	@Test
	void POSTcall_WithInCorrectData_ReturnsBadRequest() throws URISyntaxException{
		final String baseUrl = "http://localhost:"+randomServerPort+"/create";
        URI uri = new URI(baseUrl);
        URLMappingRequest requestData = new URLMappingRequest("wrongData");
        HttpEntity<URLMappingRequest> request = new HttpEntity<>(requestData);
        ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);
        assertEquals(400, result.getStatusCodeValue());
	}

	@Test
	void GETcall_WithExistingToken_RedirectsAndReturnsOK() throws URISyntaxException{
		// Create entry
		final String baseUrl = "http://localhost:"+randomServerPort+"/create";
        URI uri = new URI(baseUrl);
        URLMappingRequest requestData = new URLMappingRequest("http://google.com");
        HttpEntity<URLMappingRequest> request = new HttpEntity<>(requestData);
        ResponseEntity<URLMapping> result = this.restTemplate.postForEntity(uri, request, URLMapping.class);

		// Test GET
		String testUrl = "http://localhost:"+randomServerPort+"/" + result.getBody().getToken();
        uri = new URI(testUrl);
        ResponseEntity<String> getResult = this.restTemplate.getForEntity(uri, String.class);
		// TODO use Apache Http Client to really test the redirection status
        assertEquals(200, getResult.getStatusCodeValue());
        assertEquals(true, getResult.getBody().contains("<title>Google</title>"));
	}

	@Test
	void GETcall_WithNonExistingToken_ReturnsNotFound() throws URISyntaxException{
		// Test GET
		String testUrl = "http://localhost:"+randomServerPort+"/aaaaaa";
        URI uri = new URI(testUrl);
        ResponseEntity<String> getResult = this.restTemplate.getForEntity(uri, String.class);
        assertEquals(404, getResult.getStatusCodeValue());
	}

	@Test
	void GETcall_WithIncorrectToken_ReturnsBadRequest() throws URISyntaxException{
		// Test GET
		String testUrl = "http://localhost:"+randomServerPort+"/abcdefg";
        URI uri = new URI(testUrl);
        ResponseEntity<String> getResult = this.restTemplate.getForEntity(uri, String.class);
        assertEquals(400, getResult.getStatusCodeValue());
	}

}
