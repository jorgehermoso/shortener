package com.onesockpirates.assignment.managers;

import org.junit.jupiter.api.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.junit.jupiter.api.Disabled;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.net.URL;
import java.util.Date;

import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Matchers.*;

import com.onesockpirates.assignment.exceptions.*;
import com.onesockpirates.assignment.helpers.IHasher;
import com.onesockpirates.assignment.models.URLMapping;
import com.onesockpirates.assignment.repositories.IURLRepository;

import org.junit.jupiter.api.BeforeEach;

public class URLMappingManagerTests {

    private URLMappingManager fixture;
    private IURLRepository repository;
    private IHasher hasher;

    @BeforeEach
    public void setup(){
        repository = mock(IURLRepository.class);
        hasher = mock(IHasher.class);
        fixture = new URLMappingManager(repository, hasher);
    }


    @Test
    public void Create_WithCorrectLongURL_GeneratesObject() throws Exception{
        String testUrl = "https://google.com";

        when(repository.save(any(URLMapping.class))).then(returnsFirstArg());
        when(hasher.uniqueHash(anyString())).thenReturn("xxxxxxx");
        URLMapping result = fixture.createNew(testUrl);
        assertEquals(testUrl, result.getLongURL().toString());
        assertEquals("xxxxxx", result.getToken());
    }

    @Test
    public void Create_WithIncorrectLongURL_ThrowsException() throws Exception{
        String testUrl = "notAuRl@com";

        when(repository.save(any(URLMapping.class))).then(returnsFirstArg());
        when(hasher.uniqueHash(anyString())).thenReturn("xxxxxxx");
        assertThrows(NotAValidURLException.class, () -> {
            fixture.createNew(testUrl);
        });
    }

    @Test
    public void Create_WithEmptyLongURL_ThrowsException() throws Exception{
        String testUrl = "";

        when(repository.save(any(URLMapping.class))).then(returnsFirstArg());
        when(hasher.uniqueHash(anyString())).thenReturn("xxxxxxx");
        assertThrows(NotAValidURLException.class, () -> {
            fixture.createNew(testUrl);
        });
    }

    @Test
    public void Create_WithAlreadyShortenedLongURL_ThrowsException() throws Exception{
        String testUrl = "https://localhost/abcdef";

        when(repository.save(any(URLMapping.class))).then(returnsFirstArg());
        when(hasher.uniqueHash(anyString())).thenReturn("xxxxxxx");
        assertThrows(AlreadyEncodedURLException.class, () -> {
            fixture.createNew(testUrl);
        });
    }

    @Test
    public void Create_WithRepeatedToken_ThrowsException() throws Exception{
        String testUrl = "https://google.com";

        when(repository.save(any(URLMapping.class))).thenThrow(NotUniqueIDException.class);
        when(hasher.uniqueHash(anyString())).thenReturn("xxxxxxx");
        assertThrows(NotUniqueIDException.class, () -> {
            fixture.createNew(testUrl);
        });
    }

    @Disabled("Test not ready yet")
    @Test
    public void Create_WithRepeatedTokenAndRetry_GeneratesObject() throws Exception{
        String testUrl = "https://google.com";
        URLMapping dummy1 = new URLMapping(testUrl, "abcdef");
        URLMapping dummy2 = new URLMapping(testUrl, "bcdefg");

        when(repository.save(dummy1)).thenThrow(NotUniqueIDException.class);
        when(repository.save(dummy2)).then(returnsFirstArg());
        when(hasher.uniqueHash(anyString())).thenReturn("abcdefgh");
        URLMapping result = fixture.createNew(testUrl);
        assertEquals(testUrl, result.getLongURL().toString());
        assertEquals("bcdefg", result.getToken());
    }

    @Test
    public void Get_WithExistingToken_ReturnsURL() throws Exception{
        URLMapping dummy = new URLMapping("https://google.com", "abcdef");

        when(repository.findById(anyString())).thenReturn(dummy);
        URL result = fixture.getMapping(dummy.getToken());
        assertEquals(dummy.getLongURL(), result);
    }
    @Test
    public void Get_WithExistingToken_SetsLastUsedDate() throws Exception{
        URLMapping dummy = new URLMapping("https://google.com", "abcdef");
        Date newDate;

        when(repository.findById(anyString())).thenReturn(dummy);
        when(repository.save(any(URLMapping.class))).then(new Answer<URLMapping>() {
            public URLMapping answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                URLMapping received = (URLMapping)args[0];
                assertNotEquals(null, received.getLastUsed());
                return received;
            }});
        URL result = fixture.getMapping(dummy.getToken());
        assertEquals(dummy.getLongURL(), result);
    }
    @Test
    public void Get_WithNonExistingToken_ThrowsException() throws Exception{

        when(repository.findById(anyString())).thenReturn(null);
        assertThrows(URLNotFoundException.class, () -> {
            fixture.getMapping("abcdef");
        });
    }
    @Test
    public void Get_WithInvalidToken_ThrowsException() throws Exception{

        when(repository.findById(anyString())).thenReturn(null);
        assertThrows(NotAValidTokenException.class, () -> {
            fixture.getMapping("abcdefg");
        });
    }
    
}
